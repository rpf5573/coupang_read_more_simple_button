<?php
/**
 * Plugin Name: Read More Simple Button
 * Plugin URI: https://kmong.com/gig/155175
 * Description: 더보기 버튼 플러그인
 * Version: 1.0
 * Author: WPMate
 * Author URI: https://kmong.com/gig/155175
 */

if (!defined('ABSPATH'))
  exit;

// require 'vendor/autoload.php';

if (!defined('MBAIO_DIR')) {
  require "vendor/meta-box/meta-box/meta-box.php";
  require "vendor/meta-box/mb-settings-page/mb-settings-page.php";
  require "vendor/meta-box/meta-box-aio/meta-box-aio.php";
  require "vendor/meta-box/meta-box-tooltip/meta-box-tooltip.php";
}

$plugin_slug = 'read_more_simple_button';
$option_name = $plugin_slug;

add_action( 'wp_enqueue_scripts', function (){
  wp_enqueue_script('jquery');
  wp_enqueue_script('jquery-cookie-plugin', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js', array('jquery'), null, true);
  wp_enqueue_style('read-more-simple-button-style', plugin_dir_url(__FILE__) . 'assets/css/main.css');
});

add_filter('mb_settings_pages', function ($settings_pages) {
  global $option_name;

  $settings_pages[] = [
    'id' => $option_name,
    'option_name' => $option_name,
    'menu_title' => '포스트 더보기 버튼 설정',
    'parent' => 'tools.php',
  ];
  return $settings_pages;
});

add_filter('rwmb_meta_boxes', function ($meta_boxes) {
  global $option_name;

  $meta_boxes[] = [
    'id' => $option_name . '_options',
    'title' => '더보기 버튼 설정',
    'context' => 'normal',
    'settings_pages' => $option_name,
    'tab' => 'general',
    'fields' => [
      [
        'name' => '쿠키 만료 시간 (분단위)',
        'id' => 'cookie_expire',
        'type' => 'number',
        'min' => 0,
        'step' => 1,
        'std' => 120,
        'label_description' => '쿠키 만료 시간을 설정합니다.',
        'tooltip' => '예를들어 120을 입력하면,  후에 다시 더보기 버튼이 보입니다'
      ],
      [
        'name' => '버튼 링크',
        'id' => 'button_link',
        'type' => 'text',
        'label_description' => '버튼 눌렀을때 이동할 링크를 설정합니다',
      ],
      [
        'name' => '더보기 문구',
        'id' => 'read_more_button_text',
        'type' => 'text',
        'label_description' => '더보기 버튼의 문구를 설정합니다',
      ],
      [
        'name' => '가림막 높이',
        'id' => 'read_more_curtain_height',
        'type' => 'text',
        'label_description' => '가림막 높이를 설정합니다. 400px, 50vh 등 단위를 포함해서 입력해주세요',
      ],
    ],
  ];
  return $meta_boxes;
});

add_filter('the_content', function ($content) {
  global $option_name;

  // post 상세 페이지에서만 작동한다
  if (!is_singular('post')) {
    return $content;
  }
  $post_id = get_the_ID();

  $cookie_expire = rwmb_get_value('cookie_expire', ['object_type' => 'setting'], $option_name) ?: 120;
  $button_link = rwmb_get_value('button_link', ['object_type' => 'setting'], $option_name) ?: '/';
  $read_more_button_text = rwmb_get_value('read_more_button_text', ['object_type' => 'setting'], $option_name) ?: '링크 클릭하고 더보기';

  ob_start(); ?>
  <div id="read-more-button-js-data" data-cookie_expire="<?php echo $cookie_expire; ?>" data-link_url="<?php echo $button_link; ?>" data-curtain_height="<?php echo $read_more_curtain_height; ?>"></div>
  <div class="continue_post" data-post-id="<?php echo $post_id; ?>" data-role="btnMoreReadDynamic" style="">
    <div class="continue_post_wrap">
      <button class="read_more_button" type="button"><?php echo $read_more_button_text; ?></button>
    </div>
  </div>
  <?php
  $new_content = ob_get_clean() . $content;
  return $new_content;
}, 999999999999);

// 커튼 높이 설정 적용
add_action( 'wp_head', function() {
  global $option_name;
  
  $read_more_curtain_height = rwmb_get_value('read_more_curtain_height', ['object_type' => 'setting'], $option_name) ?: '400px';
  if (ctype_digit($read_more_curtain_height)) {
    $read_more_curtain_height .= 'px';
  }
  ?>
  <style>
    .single-post .post.has-post-thumbnail {
      max-height: <?php echo $read_more_curtain_height; ?>;
      height: <?php echo $read_more_curtain_height; ?>;
    }
  </style>
  <?php
});

add_action('wp_footer', function () {

  // post 상세 페이지에서만 작동한다
  if (!is_singular('post')) {
    return;
  } ?>

  <script type="text/javascript">
    (($) => {
      // 이거는 한개밖에 없다 어차피
      const coupang_ad = $('.continue_post');
      if (coupang_ad.length === 0) return;

      const post_id = $(coupang_ad).data('post-id');
      if (!post_id) return;

      const cookie_key = `read_more_button_${post_id}`;
      const $post = $('.single-post .has-post-thumbnail');
      if ($post === null) return;

      $(document).ready(() => {
        const checkCookie = $.cookie(cookie_key);
        const $button = $('.read_more_button');
        const $data = $('#read-more-button-js-data');
        const cookie_expire = Number($data.data('cookie_expire'));
        const button_text = $data.data('read_more_button_text');
        const button_link = $data.data('link_url');
        const curtain_height = $data.data('curtain_height');

        // 쿠키가 있으면 뭐 할게 없어
        if (checkCookie) {
          $('body').addClass('full-view');
          return;
        }

        // 쿠키가 없으면 CSS는 그대로 두고 이벤트만 건다
        const openFullView = () => {
          // 글을 보여주고
          $('body').addClass('full-view');

          // 쿠키를 만들어서 120분 동안 보이지 않게 한다
          const expDate = new Date();
          expDate.setTime(expDate.getTime() + (cookie_expire * 60 * 1000));
          $.cookie(cookie_key, 'value', { expires: expDate, path: '/' });
          window.open(button_link, '_blank');
        };

        $button.on('click', openFullView);
      });
    })(jQuery);
  </script>
  <?php

}, 999999);
